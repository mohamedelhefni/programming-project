package com.banking.dao;

import com.banking.models.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO {

    private static final String tableName = "users";
    private static final String usernameColumn = "username";
    private static final String emailColumn = "email";
    private static final String passwordColumn = "password";
    private static final String checkingBalanceColumn = "checking_balance";
    private static final String savingBalanceColumn = "saving_balance";

    private static final String isAdminColumn = "is_admin";
    private static final String idColumn = "id";

    private static final ObservableList<User> users;

    static {
        users = FXCollections.observableArrayList();
        updateUsersFromDB();
    }

    public static ObservableList<User> getUsers() {
        return FXCollections.unmodifiableObservableList(users);
    }

    private static void updateUsersFromDB() {

        String query = "SELECT * FROM " + tableName;

        try (Connection connection = Database.connect()) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            users.clear();
            while (rs.next()) {
                users.add(new User(
                        rs.getInt(idColumn),
                        rs.getString(usernameColumn),
                        rs.getString(emailColumn),
                        rs.getString(passwordColumn),
                        rs.getFloat(checkingBalanceColumn),
                        rs.getFloat(savingBalanceColumn),
                        rs.getBoolean(isAdminColumn)
                ));
            }
        } catch (SQLException e) {
            Logger.getAnonymousLogger().log(
                    Level.SEVERE,
                    LocalDateTime.now() + ": Could not load Users from database ");
            users.clear();
        }
    }

    public static void update(User newUser) {
        //udpate database
        int rows = CRUDHelper.update(
                tableName,
                new String[]{usernameColumn, emailColumn, passwordColumn, savingBalanceColumn, checkingBalanceColumn, isAdminColumn},
                new Object[]{newUser.getUsername(), newUser.getEmail(), newUser.getPassword(), newUser.getSavingBalance(), newUser.getCheckingBalance(), newUser.getIsAdmin()},
                new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.FLOAT, Types.FLOAT, Types.BOOLEAN},
                idColumn,
                Types.INTEGER,
                newUser.getId()
        );

        if (rows == 0)
            throw new IllegalStateException("User to be updated with id " + newUser.getId() + " didn't exist in database");

        //update cache
        Optional<User> optionalUser = getUser(newUser.getId());
        optionalUser.ifPresentOrElse((oldUser) -> {
            users.remove(oldUser);
            users.add(newUser);
        }, () -> {
            throw new IllegalStateException("User to be updated with id " + newUser.getId() + " didn't exist in database");
        });
    }

    public static void insertUser(String username, String email, String password, float checkingBalance, float savingBalance, boolean isAdmin) {
        int id = (int) CRUDHelper.create(
                tableName,
                new String[]{"username", "email", "password", "saving_balance", "checking_balance", "is_admin"},
                new Object[]{username, email, password, checkingBalance, savingBalance, isAdmin},
                new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,  Types.FLOAT, Types.FLOAT, Types.BOOLEAN});

        //update cache
        users.add(new User(
                id,
                username,
                email,
                password,
                checkingBalance,
                savingBalance,
                 isAdmin
        ));
    }

    public static void delete(int id) {
        //update database
        CRUDHelper.delete(tableName, id);

        //update cache
        Optional<User> user = getUser(id);
        user.ifPresent(users::remove);

    }

    public static Optional<User> getUser(int id) {
        for (User user : users) {
            if (user.getId() == id) return Optional.of(user);
        }
        return Optional.empty();
    }

    public static Optional<User> getUserByEmail(String email) throws SQLException {
        User user;
        String query ="SELECT * FROM  " + tableName + " WHERE  email = '" + email + "' LIMIT 1";
        try (Connection connection = Database.connect()) {
            PreparedStatement statement = connection.prepareStatement(query);
            try (ResultSet rs = statement.executeQuery()) {
                user =  new User(rs.getInt(idColumn), rs.getString(usernameColumn), rs.getString(emailColumn), rs.getString(passwordColumn), rs.getFloat(checkingBalanceColumn), rs.getFloat(savingBalanceColumn), rs.getBoolean(isAdminColumn));
            }
        }

        return Optional.of(user);
    }

    public static Optional<User> getUserById(int id) throws SQLException {
        User user;
        String query ="SELECT * FROM  " + tableName + " WHERE  id = '" + id + "' LIMIT 1";
        try (Connection connection = Database.connect()) {
            PreparedStatement statement = connection.prepareStatement(query);
            try (ResultSet rs = statement.executeQuery()) {
                user =  new User(rs.getInt(idColumn), rs.getString(usernameColumn), rs.getString(emailColumn), rs.getString(passwordColumn), rs.getFloat(checkingBalanceColumn), rs.getFloat(savingBalanceColumn), rs.getBoolean(isAdminColumn));
            }
        } catch (SQLException exception) {
            Logger.getAnonymousLogger().log(
                    Level.SEVERE,
                    LocalDateTime.now() + ": Could not fetch from " + tableName + " exc: " + exception.toString() );
            return null;
        }

        return Optional.of(user);
    }


}

