package com.banking.dao;

import com.banking.models.Transaction;
import com.banking.models.User;
import com.banking.utils.UserSession;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransactionDAO {

    private static final String tableName = "transactions";
    private static final String fromColumn = "from_id";
    private static final String toColumn = "to_id";
    private static final String amountColumn = "amount";
    private static final String messageColumn = "message";
    private static final String createdAtColumn = "created_at";
    private static final String idColumn = "id";

    private static User user;
    private static final ObservableList<Transaction> transactions;
    private boolean toOnly = false;

    static {
        transactions = FXCollections.observableArrayList();
    }

    public static ObservableList<Transaction> getTransactions() {
        return FXCollections.unmodifiableObservableList(transactions);
    }

    public TransactionDAO(User user, boolean toOnly) {
        if (toOnly) {
            this.toOnly = true;
        }
        this.user = user;
        this.updateTransactionsFromDB();
    }

    private void updateTransactionsFromDB() {
        String query;
        if (toOnly) {
            query = "SELECT * FROM " + tableName + " WHERE " + " to_id = " + user.getId() + " ORDER BY created_at DESC LIMIT 5 ";
        } else {
            query = "SELECT * FROM " + tableName + " WHERE from_id = " + user.getId() + " OR to_id = " + user.getId() + " ORDER BY created_at DESC LIMIT 5 ";
        }
        try (Connection connection = Database.connect()) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            transactions.clear();
            while (rs.next()) {
                Optional<User> fromUser = UserDAO.getUserById(rs.getInt(fromColumn));
                Optional<User> toUser = UserDAO.getUserById(rs.getInt(toColumn));
                transactions.add(
                        new Transaction(
                                rs.getInt(idColumn),
                                fromUser.get(),
                                toUser.get(),
                                rs.getFloat(amountColumn),
                                rs.getString(messageColumn),
                                rs.getTimestamp(createdAtColumn)
                        )
                );
            }
        } catch (SQLException e) {
            Logger.getAnonymousLogger().log(
                    Level.SEVERE,
                    LocalDateTime.now() + ": Could not load Transactions from database exception: " + e.toString());
            transactions.clear();
        }
    }

    public static boolean insertTransaction(User from, User to, float amount, String message) throws SQLException {
        if (amount > from.getCheckingBalance() || amount <= 0) {
            return false;
        }

        if (!to.deposit(amount)) {
            return false;
        }

        if (!from.withdraw(amount)) {
            return false;
        }


        int id = (int) CRUDHelper.create(
                tableName,
                new String[]{"from_id", "to_id", "amount", "message"},
                new Object[]{from.getId(), to.getId(), amount, message},
                new int[]{Types.INTEGER, Types.INTEGER, Types.FLOAT, Types.VARCHAR});

        UserSession.user.setSavingBalance(from.getSavingBalance());
        UserSession.user.setCheckingBalance(from.getCheckingBalance());

        transactions.add(new Transaction(id, from, to, amount, message, new Date()));
        return id > 0;
    }


}
