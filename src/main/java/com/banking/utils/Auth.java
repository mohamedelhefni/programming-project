package com.banking.utils;

import com.banking.controllers.Navigate;

import java.io.IOException;

public class Auth {
    public static void Logout(javafx.event.ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        UserSession.user = null;
        navigate.To("login.fxml");
    }
}
