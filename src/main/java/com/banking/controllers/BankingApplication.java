package com.banking.controllers;

import com.banking.dao.Database;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class BankingApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        if (Database.isOK()) {
            FXMLLoader fxmlLoader = new FXMLLoader(BankingApplication.class.getResource("login.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 320, 240);
            stage.setTitle("Login");
            stage.setScene(scene);
            stage.show();
        } else {
            System.out.println("Database Error");
        }


    }

    public static void main(String[] args) {
        launch();
    }
}