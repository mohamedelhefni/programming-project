package com.banking.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class Navigate {

    private javafx.event.ActionEvent  event;
    public  Navigate(javafx.event.ActionEvent e) {
        this.event = e;
    }
    public void To( String target) throws IOException {
        Parent wn = FXMLLoader.load(getClass().getResource(target));
        Scene scene = new Scene(wn);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
