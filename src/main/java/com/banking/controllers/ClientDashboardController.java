package com.banking.controllers;

import com.banking.dao.UserDAO;
import com.banking.models.User;
import com.banking.utils.Auth;
import com.banking.utils.UserSession;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.io.IOException;
import java.sql.SQLException;

public class ClientDashboardController {
    private User user;

     @FXML
     private Label name;

    @FXML
    private Button logout;

    @FXML
    private Button checkingAccount;

    @FXML
    private Button savingAccount;


    public void initialize() throws SQLException {
        user = UserSession.user;
        name.setText("Welcome Mr. " + user.getUsername() + " Your balance is : " + user.getBalance() + " EGP") ;
    }

    @FXML
    public void handleLogout(ActionEvent event) throws IOException {
        Auth.Logout(event);
    }

    @FXML
    public void handleSavingAccount(ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        navigate.To("saving-account.fxml");
    }
    @FXML
    public void handleCheckingAccount(ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        navigate.To("checking-account.fxml");
    }

}
