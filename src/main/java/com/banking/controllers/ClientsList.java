package com.banking.controllers;

import com.banking.dao.UserDAO;
import com.banking.models.User;
import com.banking.utils.Auth;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class ClientsList {
    @FXML
    private ListView list;
    @FXML
    private Button Add;
    @FXML
    private Button Edit;

    @FXML
    private Button account;
    @FXML
    private Button Logout;

    @FXML
    private Button Delete;

    public void initialize() {
        ObservableList<User> data = UserDAO.getUsers();
        list.setItems(data);
    }


    @FXML
    public void onAddClick(ActionEvent event)  {
        Dialog<User> addDialog = createClientDialog(null);
        Optional<User> result = addDialog.showAndWait();
        result.ifPresent(user -> {
            UserDAO.insertUser(user.getUsername(),user.getEmail(), user.getPassword(), user.getCheckingBalance(), user.getSavingBalance(), user.getIsAdmin());
        });
        event.consume();
    }

    @FXML
    public void onEditClick(ActionEvent event)  {
        User c = (User) list.getSelectionModel().getSelectedItem();
        Dialog<User> addDialog = createClientDialog(c);
        Optional<User> result = addDialog.showAndWait();
        result.ifPresent(user -> {
            UserDAO.update(user);
        });
        event.consume();

    }

    @FXML
    public void onDeleteClick(ActionEvent event)  {
        User c = (User) list.getSelectionModel().getSelectedItem();
        UserDAO.delete(c.getId());
    }

    @FXML
    public void onLogoutClick(ActionEvent event) throws IOException {
        Auth.Logout(event);
    }
    @FXML
    public void redirectToAccount(ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        navigate.To("dashboard.fxml");
    }


    private Dialog<User> createClientDialog(User user) {
        //create the dialog itself
        Dialog<User> dialog = new Dialog<>();
        dialog.setTitle("Add Dialog");
        if(user ==null){
            dialog.setHeaderText("Add a new client");
        } else {
            dialog.setHeaderText("Edit a client record");
        }
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Stage dialogWindow = (Stage) dialog.getDialogPane().getScene().getWindow();

        //create the form for the user to fill in
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        TextField username = new TextField();
        username.setPromptText("Username");
        TextField email = new TextField();
        email.setPromptText("Email");
        TextField password = new PasswordField();
        password.setPromptText("Password");
        TextField savingBalance = new TextField();
        savingBalance.setPromptText("Saving Balance");
        TextField checkingBalance = new TextField();
        checkingBalance.setPromptText("Checking Balance");
        CheckBox isAdmin = new CheckBox();
        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Email:"), 0, 1);
        grid.add(email, 1, 1);
        grid.add(new Label("Password:"), 0, 2);
        grid.add(password, 1, 2);
        grid.add(new Label("Saving Balance:"), 0, 3);
        grid.add(savingBalance, 1, 3);
        grid.add(new Label("Checking Balance:"), 0, 4);
        grid.add(checkingBalance, 1, 4);
        grid.add(new Label("Is Admin:"), 0, 5);
        grid.add(isAdmin, 1, 5);
        dialog.getDialogPane().setContent(grid);


        //disable the OK button if the fields haven't been filled in
        dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(
                Bindings.createBooleanBinding(() -> username.getText().trim().isEmpty(), username.textProperty())
                        .or(Bindings.createBooleanBinding(() -> email.getText().trim().isEmpty(), email.textProperty())
                                .or(Bindings.createBooleanBinding(() -> savingBalance.getText().trim().isEmpty(), savingBalance.textProperty())
                                        .or(Bindings.createBooleanBinding(() -> checkingBalance.getText().trim().isEmpty(), checkingBalance.textProperty())
                                ))));


        //make sure the dialog returns a Person if it's available
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                int id = -1;
                if (user != null) id = user.getId();
                return new User(id, username.getText(), email.getText(), password.getText(), Float.valueOf(checkingBalance.getText()), Float.valueOf(savingBalance.getText()), Boolean.valueOf(isAdmin.isSelected()));
            }
            return null;
        });

        //if a record is supplied, use it to fill in the fields automatically
        if (user != null) {
            username.setText(user.getUsername());
            email.setText(user.getEmail());
            checkingBalance.setText(String.valueOf(user.getCheckingBalance()));
            savingBalance.setText(String.valueOf(user.getSavingBalance()));
            isAdmin.setSelected(user.getIsAdmin());
        }

        return dialog;
    }

}
