//package com.example.bankingsystem;
//
//import com.example.models.Client;
//import javafx.beans.property.ReadOnlyStringProperty;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.cell.PropertyValueFactory;
//
//import java.io.IOException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.ResourceBundle;
//
//public class ClientsController implements Initializable {
//    @FXML
//    private TableView<Client> clients;
//    @FXML
//    public TableColumn<Client, Integer> id;
//
//    @FXML
//    public TableColumn<Client, String> username;
//
//    @FXML
//    public TableColumn<Client, String> email;
//
//    @FXML
//    public TableColumn<Client, Float> balance;
//
//    @FXML
//    protected void onSelectRecord() throws IOException {
//        System.out.println(clients.getSelectionModel().getSelectedItems().get(0).getUserName());
//    }
//
//
//    @Override
//    public void initialize(URL url, ResourceBundle resourceBundle) {
//
//
//        id.setCellValueFactory(new PropertyValueFactory<>("id"));
//        username.setCellValueFactory(new PropertyValueFactory<>("userName"));
//        email.setCellValueFactory(new PropertyValueFactory<>("email"));
//        balance.setCellValueFactory(new PropertyValueFactory<>("balance"));
//
//        ArrayList<Client> Clients = new ArrayList<Client>();
//        Clients.add(new Client(1,"Amos", "Chepchieng", 10));
//        Clients.add(new Client(2,"test", "Chepchieng", 10));
//        clients.getItems().setAll(Clients);
//
//    }
//    private ObservableList<Client> Clients =  FXCollections.observableArrayList(
//            new Client(1,"Amos", "Chepchieng", 10),
//            new Client(2,"Keep", "Too", 20)
//    );
//    // add your data here from any source
////    private ObservableList<StudentsModel> studentsModels = FXCollections.observableArrayList(
////            new StudentsModel(1,"Amos", "Chepchieng"),
////            new StudentsModel(2,"Keep", "Too"),
////    );
//}
