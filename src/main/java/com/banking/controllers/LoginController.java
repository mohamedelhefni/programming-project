package com.banking.controllers;

import com.banking.dao.UserDAO;
import com.banking.models.User;
import com.banking.utils.UserSession;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class LoginController {
    @FXML
    private Label error;

   @FXML
   private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    @FXML
    protected void onLoginClick(javafx.event.ActionEvent event) throws IOException, SQLException {
        Navigate navigator = new Navigate(event);
        String email = usernameField.getText();
        String password = passwordField.getText();
        Optional<User> user = UserDAO.getUserByEmail(email);
        if(user == null) {
            error.setText("username or password is not correct");
            return;
        }
        if(!email.equals(user.get().getEmail()) && !password.equals(user.get().getPassword())) {
            error.setText("username or password is not correct");
            return;
        }

        UserSession.user =user.get();

        if(user.get().getIsAdmin()) {
            navigator.To("clients-list.fxml");
        }else {
            navigator.To("dashboard.fxml");
        }

    }


}
