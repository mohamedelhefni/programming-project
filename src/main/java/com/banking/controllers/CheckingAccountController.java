package com.banking.controllers;

import com.banking.dao.TransactionDAO;
import com.banking.dao.UserDAO;
import com.banking.models.Transaction;
import com.banking.models.User;
import com.banking.utils.UserSession;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

public class CheckingAccountController {
    @FXML
    private Label checkingBalance;

    @FXML
    private Label error;

    @FXML
    private Button back;

    @FXML
    private Button transfer;

    @FXML
    private ListView transactionsList;


    public void initialize() throws SQLException {
        User user = UserSession.user;
        checkingBalance.setText("Checking balance is: " + user.getDBCheckingBalance());
        ObservableList<Transaction> data = new TransactionDAO(user ,false).getTransactions();

        transactionsList.setItems(data);
    }

    @FXML
    public void handleBack(ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        navigate.To("dashboard.fxml");
    }

    @FXML
    public void handleTransfer(ActionEvent event) throws IOException, SQLException {
        Dialog<Transaction> addDialog = createTransacitonDialog(null);
        Optional<Transaction> result = addDialog.showAndWait();
        result.ifPresent(t -> {
            if(t.getTo().getEmail() == null) {
                error.setText("something went wrong");
                return;
            }
            try {
                if(!TransactionDAO.insertTransaction(t.getFrom(), t.getTo(), t.getAmount(), t.getMessage())) {
                    error.setText("Invalid Amount of money");
                }
                checkingBalance.setText("Checking Balance: " + t.getFrom().getDBCheckingBalance());
            } catch (SQLException e) {
                error.setText("something went wrong");
            }
        });
        event.consume();
    }

    private Dialog<Transaction> createTransacitonDialog(Transaction transaction) {
        //create the dialog itself
        Dialog<Transaction> dialog = new Dialog<>();
        dialog.setTitle("Transfer Money");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Stage dialogWindow = (Stage) dialog.getDialogPane().getScene().getWindow();

        //create the form for the user to fill in
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField email = new TextField();
        email.setPromptText("Email");
        TextField amount = new TextField();
        amount.setPromptText("Amount");
        TextField message = new TextField();
        message.setPromptText("Message");
        grid.add(new Label("Email:"), 0, 1);
        grid.add(email, 1, 1);
        grid.add(new Label("Amount: "), 0, 2);
        grid.add(amount, 1, 2);
        grid.add(new Label("Messsage: "), 0, 3);
        grid.add(message, 1, 3);
        dialog.getDialogPane().setContent(grid);


        //disable the OK button if the fields haven't been filled in
        dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(
                Bindings.createBooleanBinding(() -> email.getText().trim().isEmpty(), email.textProperty())
                        .or(Bindings.createBooleanBinding(() -> amount.getText().trim().isEmpty(), amount.textProperty())
                        ));


        //make sure the dialog returns a Person if it's available
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                int id = -1;
                User from = UserSession.user;
                Optional<User> to = null;
                try {
                    to = UserDAO.getUserByEmail(email.getText());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                if(to == null) {
                    return null;
                }
                return new Transaction(id, from, to.get(), Float.parseFloat(amount.getText()), message.getText(), new Date());

            }
            return null;
        });

        return dialog;
    }
}
