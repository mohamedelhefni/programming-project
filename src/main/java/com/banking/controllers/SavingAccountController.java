package com.banking.controllers;

import com.banking.dao.TransactionDAO;
import com.banking.models.Transaction;
import com.banking.models.User;
import com.banking.utils.UserSession;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.io.IOException;
import java.sql.SQLException;

public class SavingAccountController {
    @FXML
    private Label savingBalance;

    @FXML
    private Button back;

    @FXML
    private ListView transactions;

    @FXML
    private ListView messages;


    public void initialize() throws SQLException {
        User user = UserSession.user;
        savingBalance.setText("Saving balance is: " + user.getDBSavingBalance());

        ObservableList<Transaction> transactionsData = new TransactionDAO(user, false).getTransactions();
        ObservableList<Transaction> data = new TransactionDAO(user, true).getTransactions();
        ObservableList<String> msgsData = FXCollections.observableArrayList();
        data.forEach(transaction -> {
            msgsData.add(transaction.getFrom().getEmail() + ": " + transaction.getMessage());
        });
        transactions.setItems(transactionsData);
        messages.setItems(msgsData);
    }

    @FXML
    public void handleBack(ActionEvent event) throws IOException {
        Navigate navigate = new Navigate(event);
        navigate.To("dashboard.fxml");
    }


}
