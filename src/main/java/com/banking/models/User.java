package com.banking.models;

import com.banking.dao.UserDAO;

import java.sql.SQLException;
import java.util.Optional;

public class User {
    private  int id;
    private final String username;
    private final String password;
    private final String email;
    private float checkingBalance;

    private float savingBalance;
    private final boolean isAdmin;

    public User(int id, String username, String email, String password, float checkingBalance, float savingBalance, boolean isAdmin) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.checkingBalance = checkingBalance;
        this.savingBalance = savingBalance;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getEmail() {
        return this.email;
    }
    public String getPassword() {
        return this.password;
    }
    public float getCheckingBalance() {
        return this.checkingBalance;
    }

    public float getSavingBalance() {
        return this.savingBalance;
    }

    public  void setSavingBalance(float amount) {
       this.savingBalance = amount ;
    }
    public  void setCheckingBalance(float amount) {
        this.checkingBalance = amount ;
    }


    public float getBalance() throws SQLException {
        return this.getDBCheckingBalance() + this.getDBSavingBalance();
    }

    public float getDBCheckingBalance() throws SQLException {
        Optional<User> user = UserDAO.getUserById(this.id);
        if(user == null) return 0;
        return user.get().getCheckingBalance();
    }

    public float getDBSavingBalance() throws SQLException {
        Optional<User> user = UserDAO.getUserById(this.id);
        if(user == null) return 0;
        return user.get().getSavingBalance();
    }

    public boolean deposit(float amount) throws SQLException {
        if(amount <=0) {
            return false;
        }

        this.checkingBalance = this.getDBCheckingBalance();
        this.savingBalance += amount;
        UserDAO.update(this);
        return true;
    }

    public boolean withdraw(float amount) throws SQLException {
        if(amount > checkingBalance || amount <= 0) {
                return false ;
        }
        this.savingBalance = this.getDBSavingBalance();
        this.checkingBalance -= amount;
        UserDAO.update(this);
        return true;
    }


    public boolean getIsAdmin() {
        return this.isAdmin;
    }

    @Override
    public String toString() {
        String label = isAdmin ? "admin" : "customer";
        float balance = checkingBalance + savingBalance;
        return "(" + id + ") [" + label + "] - " + username + " " + email + ", balance: " + balance + " ";
    }
}
