package com.banking.models;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class Transaction {
    private  int id;
    private final User from;
    private final User to;
    private final float amount;
    private final String message;
    private final Date createdAt;


    public Transaction(int id, User from, User to, float amount, String message, Date createdAt) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.message = message;
        this.createdAt = createdAt;
    }




    public int getId() {
        return this.id;
    }


    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public float getAmount() {
        return amount;
    }

    public String getMessage () {
        return message;
    }

    @Override
    public String toString() {
        String timeStamp = new SimpleDateFormat(" dd, MMM yyyy hh:mm:ss aaa").format(createdAt);

        return "(" + id + ") " + from.getEmail() + " -> " + to.getEmail() + " (" + amount + ")  [ "+ message +" ] " + timeStamp;
    }

}
