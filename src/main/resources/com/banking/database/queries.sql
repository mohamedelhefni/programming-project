CREATE TABLE "users" (
	"id"	INTEGER,
	"username"	varchar(100) NOT NULL,
	"email" UNIQUE varchar(50) NOT NULL,
	"password"	varchar(255) NOT NULL,
	"saving_balance"	REAL DEFAULT (0),
	"checking_balance"	REAL DEFAULT (0),
	"is_admin"	BOOLEAN DEFAULT false,
	PRIMARY KEY("id" AUTOINCREMENT)
);


CREATE TABLE "transactions" (
	"id"	INTEGER,
	"from_id" INTEGER NOT NULL,
	"to_id" INTEGER NOT NULL,
	"amount" REAL NOT NULL,
	"message" TEXT,
	"created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     FOREIGN KEY("from") REFERENCES users(id),
     FOREIGN KEY("to") REFERENCES users(id),
     PRIMARY KEY("id" AUTOINCREMENT)
);